<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "airports".
 *
 * @property int $id
 * @property string $city
 * @property string $name
 * @property string $iata
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Flights[] $flights
 * @property Flights[] $flights0
 */
class Airports extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'airports';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city', 'name', 'iata'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['city', 'name', 'iata'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'name' => 'Name',
            'iata' => 'Iata',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Flights]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlights()
    {
        return $this->hasMany(Flights::className(), ['from_id' => 'id']);
    }

    /**
     * Gets query for [[Flights0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlights0()
    {
        return $this->hasMany(Flights::className(), ['to_id' => 'id']);
    }

    public static function findAirport($query)
    {
    
        return Airports::find() 
                            ->where(['like', 'name', $query])
                            ->OrWhere(['like', 'city', $query])
                            ->OrWhere(['like', 'iata', $query])
                            ->one();
    
    }


}
