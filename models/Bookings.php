<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bookings".
 *
 * @property int $id
 * @property int $flight_from
 * @property int|null $flight_back
 * @property string $date_from
 * @property string|null $date_back
 * @property string $code
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Flights $flightBack
 * @property Flights $flightFrom
 * @property Passengers[] $passengers
 */
class Bookings extends \yii\db\ActiveRecord
{
    public function beforeValidate()
    {
        $code = $this->getCode();
        if (preg_match('/[0-9]/', $code) == 0) {
            $this->code = strtoupper($code);
            return true;
        } else {
            $this->beforeValidate();
        }
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bookings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['flight_from', 'date_from', 'code'], 'required'],
            [['flight_from', 'flight_back'], 'integer'],
            [['date_from', 'date_back', 'created_at', 'updated_at'], 'safe'],
            [['code'], 'string', 'max' => 5],
            [['flight_back'], 'exist', 'skipOnError' => true, 'targetClass' => Flights::className(), 'targetAttribute' => ['flight_back' => 'id']],
            [['flight_from'], 'exist', 'skipOnError' => true, 'targetClass' => Flights::className(), 'targetAttribute' => ['flight_from' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flight_from' => 'Flight From',
            'flight_back' => 'Flight Back',
            'date_from' => 'Date From',
            'date_back' => 'Date Back',
            'code' => 'Code',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    protected function getCode()
    {
        return Yii::$app->getSecurity()->generateRandomString(5);
    }

    /**
     * Gets query for [[FlightBack]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlightBack()
    {
        return $this->hasOne(Flights::className(), ['id' => 'flight_back']);
    }

    /**
     * Gets query for [[FlightFrom]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlightFrom()
    {
        return $this->hasOne(Flights::className(), ['id' => 'flight_from']);
    }

    /**
     * Gets query for [[Passengers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPassengers()
    {
        return $this->hasMany(Passengers::className(), ['booking_id' => 'id']);
    }
}
