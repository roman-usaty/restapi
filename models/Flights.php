<?php

namespace app\models;

use Yii;
use app\models\Airports;

/**
 * This is the model class for table "flights".
 *
 * @property int $id
 * @property string $flight_code
 * @property int $from_id
 * @property int $to_id
 * @property string $time_from
 * @property string $time_to
 * @property int $cost
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Bookings[] $bookings
 * @property Bookings[] $bookings0
 * @property Airports $from
 * @property Airports $to
 */
class Flights extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flights';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['flight_code', 'from_id', 'to_id', 'time_from', 'time_to', 'cost'], 'required'],
            [['from_id', 'to_id', 'cost'], 'integer'],
            [['time_from', 'time_to', 'created_at', 'updated_at'], 'safe'],
            [['flight_code'], 'string', 'max' => 10],
            [['from_id'], 'exist', 'skipOnError' => true, 'targetClass' => Airports::className(), 'targetAttribute' => ['from_id' => 'id']],
            [['to_id'], 'exist', 'skipOnError' => true, 'targetClass' => Airports::className(), 'targetAttribute' => ['to_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flight_code' => 'Flight Code',
            'from_id' => 'From ID',
            'to_id' => 'To ID',
            'time_from' => 'Time From',
            'time_to' => 'Time To',
            'cost' => 'Cost',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Bookings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBookingsBack()
    {
        return $this->hasMany(Bookings::className(), ['flight_back' => 'id']);
    }

    /**
     * Gets query for [[Bookings0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBookingsFrom()
    {
        return $this->hasMany(Bookings::className(), ['flight_from' => 'id']);
    }

    public static function getFrom($from)
    {
        $from_id = Airports::findAirport($from);

        return $from_id->id;
    }

    public static function getTo($to)
    {
        $to_id = Airports::findAirport($to);

        return $to_id->id;
    }

    public static function getFlight($from, $to)
    {
        return Flights::find() 
                            ->where(['like', 'from_id', $from])
                            ->AndWhere(['like', 'to_id', $to])
                            ->all();
    }
}
