<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passengers".
 *
 * @property int $id
 * @property int $booking_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birth_date
 * @property string $document_number
 * @property string|null $place_from
 * @property string|null $place_back
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Bookings $booking
 */
class Passengers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passengers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['booking_id', 'first_name', 'last_name', 'birth_date', 'document_number'], 'required'],
            [['booking_id'], 'integer'],
            [['birth_date', 'created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['document_number'], 'string', 'max' => 10],
            [['place_from', 'place_back'], 'string', 'max' => 3],
            [['booking_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bookings::className(), 'targetAttribute' => ['booking_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_id' => 'Booking ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'birth_date' => 'Birth Date',
            'document_number' => 'Document Number',
            'place_from' => 'Place From',
            'place_back' => 'Place Back',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Booking]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Bookings::className(), ['id' => 'booking_id']);
    }
}
