<?php 

namespace app\controllers;

use Yii;

use app\models\Users;
use app\models\Airports;
use app\models\Flights;
use app\models\Bookings;
use app\models\Passengers;


use yii\web\NotFoundHttpException;
use yii\web\Controller;


use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

function xprint($arResult)
{
    echo '<pre>';
        print_r($arResult);
    echo '</pre>';
}

class ApiController extends Controller
{

/*     public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['authenticator']['except'] = ['login', 'signup', 'index'];
        return $behaviors;
    } */

    public function actionRegister() 
    {
        $user = new Users;
        $post = Yii::$app->request->post();

        

        if ($user->load($post,"")) {
            if ($user->validate()) {

                $hash = $user->securityPassword($user->password);
                $user->password = $hash;

                $user->save();  

                return Yii::$app->response->statusCode = 204;
            } else {

                $STATUS_CODE = Yii::$app->response->statusCode = 422;
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


                $error = $user->errors;
             
                return [
                    'error' => [
                        'code' => "$STATUS_CODE",
                        'message' => 'Validation error',
                        'errors' => $error
                    ]
                ];
            }
        } else {
            $STATUS_CODE = Yii::$app->response->statusCode = 422;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


            $error = $user->errors;
            if ($error == NULL) {
                return [
                    'error' => [
                        'code' => "$STATUS_CODE",
                        'message' => 'Validation error',
                        'errors' => 'Заполните все поля'
                    ]
                ];
            }
             
            
        }
        throw new NotFoundHttpException;
        
    }

    public function actionLogin()
    {
        $post = Yii::$app->request->post();
        $user = Users::findOne(['phone' => $post['phone']]);
        


        
        if ($user != NULL) 
        {
            $phone = $user->phone;
            $hash = $user->password;

            if (!empty($post['password'] ) && Users::validatePassword($post['password'], $hash)) {
                $token = Users::generateToken();

                $user->api_token = $token;
    
                if ($user->update($runValidation = true) !== false) {
    
                    $STATUS_CODE = Yii::$app->response->statusCode = 200;
                    return $token;
                } else {
                    $STATUS_CODE = Yii::$app->response->statusCode = 422;
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $error = $user->errors;
                 
                    return [
                        'error' => [
                            'code' => $STATUS_CODE,
                            'message' => 'Validation error',
                            'errors' => $error
                        ]
                    ];
                }
            } else {
                $STATUS_CODE = Yii::$app->response->statusCode = 422;
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 
                return [
                    'error' => [
                        'code' => $STATUS_CODE,
                        'message' => 'Validation error',
                        'errors' => [
                            "password" => "password null or incorrect"
                        ]
                    ]
                ];
            }
           

           
        }else{
            $STATUS_CODE = Yii::$app->response->statusCode = 422;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $error = $user->error;
            return [
                'error' => [
                    'code' => $STATUS_CODE,
                    'message' => 'Validation error',
                    'errors' => [
                        "phone" => ["phone or password incorrect"]
                        ]
                ]
            ];
        }
    }

    public function actionAirport()
    {
        $request = Yii::$app->request;
        
        if ($request->isGet) {
            $query = $request->get('query');
            $data = Airports::findAirport($query);
            
            if (!empty($data)) {
                for ($i=0; $i < count($data); $i++) { 
                    $items[$i] = [
                        "name" => $data->name,
                        "iata" => $data->iata
                    ];
                }
                Yii::$app->response->statusCode = 200;
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                return [
                    "data" => [
                        "items" => $items
                    ]
                ];
            }
            
        }
    }

    public function actionFlight()
    {
        $request = Yii::$app->request;

        if ($request->isGet) {
            $date1 = $request->get('date1');
            $date2 = $request->get('date2');
            
            
            if ($request->get('from') != NULL & $request->get('to') != NULL) {
                $fligts_to_tr = Flights::getFlight(Flights::getFrom($request->get('from')), Flights::getTo($request->get('to')));
                $fligts_back_tr = Flights::getFlight(Flights::getFrom($request->get('to')), Flights::getTo($request->get('from')));
                
                for ($i=0; $i < count($fligts_to_tr) ; $i++) { 
                    $fligts_to_tr[$i]->from_id = Airports::findAirport($request->get('from'));
                    $fligts_to_tr[$i]->to_id = Airports::findAirport($request->get('to'));
                }

                for ($i=0; $i < count($fligts_back_tr); $i++) { 
                    $fligts_back_tr[$i]->from_id = Airports::findAirport($request->get('to'));
                    $fligts_back_tr[$i]->to_id = Airports::findAirport($request->get('from'));
                }

                for ($i=0; $i < count($fligts_to_tr); $i++) { 
                    $fligts_to[$i] = [
                        "flight_id" => $fligts_to_tr[$i]->id,
                        "flight_code" => $fligts_to_tr[$i]->flight_code,
                        "from" => [
                            "city" =>  $fligts_to_tr[$i]->from_id->city,
                            "airport" =>  $fligts_to_tr[$i]->from_id->name,
                            "iata" =>  $fligts_to_tr[$i]->from_id->iata,
                            "date" =>  $date1,
                            "time"=>  $fligts_to_tr[$i]->time_from
                        ],
                        "to"=> [
                            "city" =>  $fligts_to_tr[$i]->to_id->city,
                            "airport" =>  $fligts_to_tr[$i]->to_id->name,
                            "iata" =>  $fligts_to_tr[$i]->to_id->iata,
                            "date" =>  $date1,
                            "time" =>  $fligts_to_tr[$i]->time_to
                        ],
                        "cost" =>  $fligts_to_tr[$i]->cost
                    ];
                }
                for ($i=0; $i < count($fligts_back_tr); $i++) { 
                    $fligts_back[$i] = [
                        "flight_id" => $fligts_back_tr[$i]->id,
                        "flight_code" => $fligts_back_tr[$i]->flight_code,
                        "from" => [
                            "city" =>  $fligts_back_tr[$i]->from_id->city,
                            "airport" =>  $fligts_back_tr[$i]->from_id->name,
                            "iata" =>  $fligts_back_tr[$i]->from_id->iata,
                            "date" =>  $date2,
                            "time"=>  $fligts_back_tr[$i]->time_from
                        ],
                        "to"=> [
                            "city" =>  $fligts_back_tr[$i]->to_id->city,
                            "airport" =>  $fligts_back_tr[$i]->to_id->name,
                            "iata" =>  $$fligts_back_tr[$i]->to_id->iata,
                            "date" =>  $date2,
                            "time" =>  $fligts_back_tr[$i]->time_to
                        ],
                        "cost" =>  $fligts_back_tr[$i]->cost
                    ];
                }

                

                $STATUS_CODE = Yii::$app->response->statusCode = 200;
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                return [
                    'data' => [
                        "flights_to" => $fligts_to,
                        "flights_back" => $fligts_back_tr
                    ]
                ];
            } else {
                $STATUS_CODE = Yii::$app->response->statusCode = 422;
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if (!empty($request->get('from'))) {
                    return [
                        'error' => [
                            'code' => $STATUS_CODE,
                            'message' => 'Validation error',
                            'errors' => [
                                'from' => 'from null or incorrect',
                            ]
                        ]
                    ];
                } elseif (!empty($request->get('to'))) {
                    return [
                        'error' => [
                            'code' => $STATUS_CODE,
                            'message' => 'Validation error',
                            'errors' => [
                                'to' => 'to null or incorrect',
                            ]
                        ]
                    ];
                } else {
                    return [
                        'error' => [
                            'code' => $STATUS_CODE,
                            'message' => 'Validation error',
                            'errors' => [
                                'GET' => 'isNull',
                            ]
                        ]
                    ];
                }
                
            }   
        }
    }

    public function actionBooking()
    {
        $STATUS_CODE = Yii::$app->response->statusCode = 200;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;

        $model = new Bookings;
        $data = json_decode($request->getRawBody(), true);

        if ($model->load($data, "")) {
            if ($model->validate()) {
                return "23223";
            } else {
                xprint($model->errors);echo($model->code);die;
            }
            xprint($model);die;
        }
       
        return "3434";
    }
}