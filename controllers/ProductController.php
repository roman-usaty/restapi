<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;


class ProductController extends ActiveController
{
    public $modelClass = 'app\models\Product';

    public function actionCreate()
    {
        $model = new Product();
        if(Yii::$app->request->post() && $model->save()){
            return true;
        }
        
    }

}